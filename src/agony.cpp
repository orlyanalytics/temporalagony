#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <cmath>

#include "defines.h"
#include "graph.h"
#include "circulation.h"
#include "dynamic_graph.h"



void
computeagony(network & g, residual & r, vertex_t *canon)
{
	double delta = 0;

	uint32_t ncnt = g.nodecnt();

	network::edgevector edges(g.edgebudget());

	while (g.nodecnt() > 1) {
		double d1 = 0, d2 = 0;
		for (vertex_t *v = g.get_first(); v; v = v->next()) {
			d1 = std::max(d1, v->v.circ.bias - v->v.circ.flow);
			d2 = std::min(d2, v->v.circ.bias - v->v.circ.flow);
			//printf("%d %f\n", v->v.circ.dual, v->v.circ.bias);
		}
		double d = std::min(d1, -d2);
		if (d == 0) break;
		if (delta == 0) {
			int exp;
			if (frexp(d, &exp) == 0.5) exp--;
			delta = ldexp(1.0, exp);
		}
		else {
			while (delta*3/4 > d) delta /= 2;
		}


		printf("delta: %f %f %f %d\n", delta, d1, d2, g.nodecnt());

		if (g.nodecnt() == 1) break;

		balance(g, r, delta);
		//printf("canon: %d\n", canon->id);
		canonize(g, r, canon);

		vertex_t *master = computeblocks(g, 3*ncnt*delta);
		if (master) {
			batchcontract(g, master, edges);
			init_residual(g, r); // update residual graph
			canon = canon->v.contract.master; // update canon, if changed
		}

		/*
		for (uint32_t i = 0; i < g.nodebudget(); i++) {
			vertex_t *v = g.get(i);
			printf("FINAL: %d %.2f %.2f\n", v->v.circ.dual, v->v.circ.bias,  v->v.circ.flow);
		}
		*/
	}

	unroll_dual(g);

	/*
	for (uint32_t i = 0; i < g.nodebudget(); i++) {
		vertex_t *v = g.get(i);
		printf("DONE: %d %f\n", v->v.circ.dual, v->v.circ.bias - v->v.circ.flow);
	}
	*/


}

void
outputrank(FILE *f, network & g, uint32_t ncnt)
{
	for (uint32_t i = 0; i < ncnt; i++) {
		vertex_t *v = g.get(i);
		fprintf(f, "%d %d\n", v->v.label, v->v.circ.dual);
	}
}

void
outputrank(FILE *f, compgraph & g, uint32_t ncnt)
{
	for (uint32_t i = 0; i < ncnt; i++) {
		cvertex_t *v = g.get(i);
		fprintf(f, "%d %d\n", v->v.label, v->v.dual);
	}
}

double
score(network & g, uint32_t ncnt, uint32_t ecnt)
{
	double agony = 0;
	for (uint32_t i = 0; i < ncnt + ecnt; i++) {
		vertex_t *v = g.get(i);
		agony += v->v.circ.dual*v->v.circ.bias;
	}
	double shift = 0;
	for (uint32_t i = 0; i < ncnt; i++) {
		vertex_t *v = g.get(i);
		shift += v->v.circ.bias;
	}

	return shift - agony;
}

void
compute_smoothagony(dynamic_network & dg, double lambda, uint32_t limit, FILE *f)
{
	network *g = dynamic_to_smooth(dg, lambda, limit);

	residual r(g->nodecnt(), 2*g->edgebudget());
	init_residual(*g, r);
	vertex_t *canon = g->get(g->nodecnt() - 2);

	computeagony(*g, r, canon);

	double ag = compute_score(dg, *g);
	printf("Agony: %f \n", ag);

	weightvector flux(dg.nodecnt(), 0);
	weight_t tf = compute_flux(dg, *g, flux);
	weight_t mv = compute_maxvar(dg, *g);
	uint32_t ch = compute_changepoints(dg, *g);
	printf("Total: %f\n", ag + lambda*tf);
	printf("Flux: %d \n", tf);
	printf("Maxvar: %d \n", mv);
	printf("Changepoints: %d \n", ch);
	printf("Flux (norm): %f \n", double(tf) / dg.nodecnt());
	printf("Maxvar (norm): %f \n", double(mv) / dg.nodecnt());
	printf("Changepoints (norm): %f \n", double(ch) / dg.nodecnt());

	output_ranks(f, dg, *g);

	delete g;
}

void
compute_linfagony(dynamic_network & dg, double lambda, uint32_t limit, FILE *f)
{
	network *g = dynamic_to_linf(dg, lambda, limit);

	residual r(g->nodecnt(), 2*g->edgebudget());
	init_residual(*g, r);
	vertex_t *canon = g->get(g->nodecnt() - 2);

	computeagony(*g, r, canon);
	double ag = compute_score(dg, *g);

	printf("\nAgony: %f \n", ag);
	weightvector flux(dg.nodecnt(), 0);
	weight_t tf = compute_flux(dg, *g, flux);
	weight_t mv = compute_maxvar(dg, *g);
	uint32_t ch = compute_changepoints(dg, *g);
	printf("Flux: %d \n", tf);
	printf("Maxvar: %d \n", mv);
	printf("Changepoints: %d \n", ch);
	printf("Flux (norm): %f \n", double(tf) / dg.nodecnt());
	printf("Maxvar (norm): %f \n", double(mv) / dg.nodecnt());
	printf("Changepoints (norm): %f \n", double(ch) / dg.nodecnt());

	output_ranks(f, dg, *g);

	delete g;
}

double
upper_bound(dynamic_network & dg, uint32_t limit)
{
	doublevector change(dg.nodecnt(), std::numeric_limits<double>::infinity());
	network *g = dynamic_to_split(dg, change, limit);

	residual r(g->nodecnt(), 2*g->edgebudget());
	init_residual(*g, r);
	vertex_t *canon = g->get(g->nodecnt() - 2);
	computeagony(*g, r, canon);

	double u = compute_score(*g, 2*dg.nodecnt(), dg.edgecnt());

	delete g;

	return u;
}

// Stopping criterion only works if agony is integral,
void
compute_binaryagony(dynamic_network & dg, double ratio_bound, uint32_t limit, FILE *f)
{
	double upper = upper_bound(dg, limit);
	printf("Upper: %f\n", upper);
	double lower = 0;

	double upper_flux = 0;
	double lower_flux = double(dg.nodecnt())*dg.nodecnt()*dg.nodecnt();
	double diff_flux = lower_flux - upper_flux;

	weightvector flux(dg.nodecnt(), 0);

	network *best = 0;

	while (upper - lower >= 1 / (diff_flux * diff_flux)) {
		double mid = (upper + lower) / 2;

		network *g = dynamic_to_smooth(dg, mid, limit);
		residual r(g->nodecnt(), 2*g->edgebudget());
		init_residual(*g, r);
		vertex_t *canon = g->get(g->nodecnt() - 2);
		computeagony(*g, r, canon);
		double ag = compute_score(dg, *g);
		weight_t tf = compute_flux(dg, *g, flux);
		printf("Upper, lower: %f %f\nAgony: %f, Flux %d\n\n", upper, lower, ag, tf);

		if (tf / ag <= ratio_bound) {
			upper = mid;
			upper_flux = tf;
			if (best) delete best;
			best = g;
			printf("SAVED: %f\n", mid);
		}
		else {
			lower = mid;
			lower_flux = tf;
			delete g;
		}

		diff_flux = lower_flux - upper_flux;
		printf("Upper, lower, diff: %f %f %f %f %f\n", upper, lower, diff_flux, upper_flux, lower_flux);
	}

	if (best == 0) {
		network *g = dynamic_to_smooth(dg, upper, limit);
		residual r(g->nodecnt(), 2*g->edgebudget());
		init_residual(*g, r);
		vertex_t *canon = g->get(g->nodecnt() - 2);
		computeagony(*g, r, canon);
		best = g;
	}

	double ag = compute_score(dg, *best);
	printf("Lambda: %f\nAgony: %f\n", upper, ag);

	weight_t tf = compute_flux(dg, *best, flux);
	weight_t mv = compute_maxvar(dg, *best);
	uint32_t ch = compute_changepoints(dg, *best);
	printf("Flux: %d \n", tf);
	printf("Maxvar: %d \n", mv);
	printf("Changepoints: %d \n", ch);
	printf("Flux (norm): %f \n", double(tf) / dg.nodecnt());
	printf("Maxvar (norm): %f \n", double(mv) / dg.nodecnt());
	printf("Changepoints (norm): %f \n", double(ch) / dg.nodecnt());


	output_ranks(f, dg, *best);
	delete best;
}


void
compute_loneagony(dynamic_network & dg, double flux_bound, uint32_t limit, FILE *f)
{
	double upper = upper_bound(dg, limit);
	printf("Upper: %f\n", upper);

	doublevector lambda(dg.nodecnt(), 0);
	weightvector flux(dg.nodecnt(), 0);

	network *best = 0;
	double best_agony = upper;
	double best_lang = 0;
	uint32_t stuck_it = 0;
	
	double scale = 2;
	while (scale >= 2e-4) {
		printf("New round\n");
		network *g = dynamic_to_smooth(dg, lambda, limit);
		residual r(g->nodecnt(), 2*g->edgebudget());
		init_residual(*g, r);
		vertex_t *canon = g->get(g->nodecnt() - 2);
		computeagony(*g, r, canon);
		double ag = compute_score(dg, *g);
		compute_flux(dg, *g, flux);

		bool valid = true;
		double norm = 0;
		double lang = ag;
		for (uint32_t i = 0; i < flux.size(); i++) {
			if (flux[i] > flux_bound) valid = false;
			norm += (flux[i] - flux_bound)*(flux[i] - flux_bound);
			lang += lambda[i]*(flux[i] - flux_bound);
		}

		if (valid && ag < best_agony) {
			best_agony = ag;
			if (best) delete best;
			best = g;
		}
		else {
			delete g;
		}

		if (norm == 0) break;

		if (lang > best_lang) {
			best_lang = lang;
			stuck_it = 0;
		} else {
			stuck_it++;
		}

		if (stuck_it > 5) scale /= 2;


		for (uint32_t i = 0; i < flux.size(); i++) {
			lambda[i] += (flux[i] - flux_bound) * scale * (upper - ag) / norm;
			if (lambda[i] < 0) lambda[i] = 0;
			printf("%d (%.3f) ", flux[i], lambda[i]);
		}
		printf("\n%f %f \n", ag, lang);



	}
	/*


	printf("Agony: %f \n", compute_score(dg, *g));
	delete g;

	//output_ranks(f, dg, *g);
	*/

	if (best) {
		output_ranks(f, dg, *best);
		delete best;
	}
}


void
compute_segagony(dynamic_network & dg, uint32_t limit, FILE *f)
{
	doublevector change(dg.nodecnt());
	weightvector r1(dg.nodecnt());
	weightvector r2(dg.nodecnt());

	doublevector old_change(dg.nodecnt(), std::numeric_limits<double>::infinity());

	init_change(dg, change);
	//change[0] = 1;
	//change[1] = 2;
	double agony = 0;

	uint32_t rounds = 0;
	do {
		old_change = change;
		network *g1 = dynamic_to_split(dg, change, limit);

		residual res1(g1->nodecnt(), 2*g1->edgebudget());
		init_residual(*g1, res1);
		vertex_t *canon = g1->get(g1->nodecnt() - 2);
		computeagony(*g1, res1, canon);
		extract_ranks(*g1, dg.nodecnt(), r1, r2);
		agony = compute_score(*g1, 2*dg.nodecnt(), dg.edgecnt());
		delete g1;

		network *g2 = dynamic_to_split(dg, r1, r2);
		residual res2(g2->nodecnt(), 2*g2->edgebudget());
		init_residual(*g2, res2);
		canon = g2->get(g2->nodecnt() - 1);
		computeagony(*g2, res2, canon);
		extract_change(dg, *g2,  change);
		delete g2;
		rounds++;
	} while (old_change != change);

	printf("\nRounds: %d \n", rounds);
	printf("Agony: %f \n", agony);
	weight_t tf = compute_flux(r1, r2);
	weight_t mv = tf;
	uint32_t ch = compute_changepoints(r1, r2);
	printf("Flux: %d \n", tf);
	printf("Maxvar: %d \n", mv);
	printf("Changepoints: %d \n", ch);
	printf("Flux (norm): %f \n", double(tf) / dg.nodecnt());
	printf("Maxvar (norm): %f \n", double(mv) / dg.nodecnt());
	printf("Changepoints (norm): %f \n", double(ch) / dg.nodecnt());

	output_ranks(f, dg, r1, r2, change);
}





int
main(int argc, char **argv)
{
	static struct option longopts[] = {
		{"out",             required_argument,  NULL, 'o'},
		{"in",              required_argument,  NULL, 'i'},
		{"groups",          required_argument,  NULL, 'k'},
		{"par",             required_argument,  NULL, 'p'},
		{"method",          required_argument,  NULL, 'm'},
		{"weighted",        no_argument,        NULL, 'w'},
		{"self",            no_argument,        NULL, 's'},
		{"noscc",           no_argument,        NULL, 'C'},
		{"help",            no_argument,        NULL, 'h'},
		{ NULL,             0,                  NULL,  0 }
	};

	char *inname = NULL;
	char *outname = NULL;
	bool weighted = false;
	bool self = false;
	bool scc = true;
	uint32_t groupcnt = 0;
	double par = 0;
	char method = 's';



	int ch;
	while ((ch = getopt_long(argc, argv, "whk:o:i:sp:Cm:", longopts, NULL)) != -1) {
		switch (ch) {
			case 'h':
				printf("Usage: %s -i <input file> -o <output file> [-k <number>] [-whsC]\n", argv[0]);
				printf("  -h    print this help\n");
				printf("  -i    input file\n");
				printf("  -o    output file\n");
				printf("  -k    number of groups\n");
				printf("  -w    input file has weights\n");
				printf("  -s    allow self loops\n");
				printf("  -p    parameter for constraining flux (depends on the method)\n");
				printf("  -m    method (s = smooth, c = segment)\n");
				printf("  -C    disable strongly connected components\n");
				return 0;
				break;
			case 'i':
				inname = optarg;
				break;
			case 'm':
				method = optarg[0];
				break;
			case 'o':
				outname = optarg;
				break;
			case 'w':
				weighted = true;
				break;
			case 'k':
				groupcnt = atoi(optarg);
				break;
			case 'p':
				par = atof(optarg);
				break;
			case 's':
				self = true;
				break;
			case 'C':
				scc = false;
				break;
		}

	}
	if (inname == NULL) {
		printf("Missing input file\n");
		return 1;
	}

	uint32_t ncnt; 
	uint32_t ecnt; 
	


	FILE *f = fopen(inname, "r");
	dynamic_network *dg = read(f, weighted, self, ncnt, ecnt);
	fclose(f);

	f = stdout;
	if (outname) f = fopen(outname, "w");

	switch (method) {
		case 's':
			compute_smoothagony(*dg, par, groupcnt, f);
			break;
		case 'i':
			compute_linfagony(*dg, par, groupcnt, f);
			break;
		case '1':
			compute_loneagony(*dg, par, groupcnt, f);
			break;
		case 'b':
			compute_binaryagony(*dg, par, groupcnt, f);
			break;
		case 'c':
			compute_segagony(*dg, groupcnt, f);
			break;
		default:
			printf("UNKNOWN METHOD: check parameter -m\n");
	}

		
	if (outname) fclose(f);

	delete dg;



	return 0;
}
