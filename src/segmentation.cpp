#include "circulation.h"
#include "dynamic_graph.h"
#include <stdio.h>
#include <cmath>

network *
dynamic_to_split(dynamic_network  & dg, doublevector & change, uint32_t limit)
{
	uint32_t cnt = dg.nodecnt();
	uint32_t ecnt = dg.edgecnt();


	// 2* is needed for possible contractions
	network *g = new network(2*(2*cnt + ecnt + 2), 2*ecnt + 4*cnt + 1);

	for (uint32_t i = 0; i < 2*cnt + ecnt + 2; i++) {
		vertex_t *v = g->addnode();
		v->v.init(v);
	}

	uint32_t ind = 0;
	for (uint32_t i = 0; i < dg.edgecnt(); i++) {
		darc_t *e = dg.get_edge(i);

		uint32_t x = e->from->id;
		uint32_t y = e->to->id;


		if (e->v.time >= change[x]) x += cnt;
		if (e->v.time >= change[y]) y += cnt;
		
		vertex_t *from = g->get(x);
		vertex_t *to = g->get(y);
		from->v.label = e->from->id;
		to->v.label = e->to->id;

		vertex_t *middle = g->get(ind + 2*cnt);

		arc_t *e1 = g->addedge();
		arc_t *e2 = g->addedge();

		g->bindedge(e1, from, middle);
		g->bindedge(e2, to, middle);
		e2->v.circ.cost = 1;
		middle->v.circ.bias = -e->v.weight;
		to->v.circ.bias += e->v.weight;

		ind++;
	}


	vertex_t *src = g->get(2*cnt + ecnt);
	vertex_t *sink = g->get(2*cnt + ecnt + 1);
	for (uint32_t i = 0; i < 2*cnt; i++) {
        arc_t *e1 = g->addedge();
		arc_t *e2 = g->addedge();

		vertex_t *v = g->get(i);

		g->bindedge(e1, src, v);
		g->bindedge(e2, v, sink);
	}

	arc_t *loop = g->addedge();
	g->bindedge(loop, sink, src);
	if (limit == 0)
		loop->v.circ.cost = 2*cnt - 1;
	else
		loop->v.circ.cost = limit - 1;

	return g;
}

void
init_change(dynamic_network  & dg, doublevector & change)
{
	for (uint32_t i = 0; i < dg.edgecnt(); i++) {
		darc_t *e = dg.get_edge(i);
		if (e->v.from_tindex == e->from->v.tcnt / 2)
			change[e->from->id] = e->v.time;
		if (e->v.to_tindex == e->to->v.tcnt / 2)
			change[e->to->id] = e->v.time;
	}
}


network *
dynamic_to_split(dynamic_network  & dg, weightvector & r1, weightvector & r2)
{
	uint32_t ncnt = dg.nodecnt();

	dvertex_t *last = dg.get(dg.nodecnt() - 1);
	uint32_t cnt = last->v.tstart + last->v.tcnt;
	uint32_t ecnt = dg.edgecnt();
	uint32_t lecnt = cnt - dg.nodecnt();


	doublevector rmax(ncnt);
	doublevector rmin(ncnt);

	for (uint32_t i = 0; i < ncnt; i++) {
		rmax[i] = std::max(r1[i], r2[i]);
		rmin[i] = std::min(r1[i], r2[i]);
	}



	// 2* is needed for possible contractions
	network *g = new network(2*(3*cnt + ecnt + 1), 2*ecnt + lecnt + 6*cnt);

	for (uint32_t i = 0; i < 3*cnt + ecnt + 2; i++) {
		vertex_t *v = g->addnode();
		v->v.init(v);
	}

	uint32_t ind = 0;
	for (uint32_t i = 0; i < dg.edgecnt(); i++) {
		darc_t *e = dg.get_edge(i);

		uint32_t x = e->v.from_tindex + e->from->v.tstart;
		uint32_t y = e->v.to_tindex + e->to->v.tstart;

		uint32_t ox = e->from->id;
		uint32_t oy = e->to->id;


		double p00 = std::max(0.0, rmin[ox] - rmin[oy] + 1);
		double p10 = std::max(0.0, rmax[ox] - rmin[oy] + 1);
		double p01 = std::max(0.0, rmin[ox] - rmax[oy] + 1);
		double p11 = std::max(0.0, rmax[ox] - rmax[oy] + 1);
		double w = e->v.weight*(p10 - p00 - p11 + p01);
		//printf("edges: %f\n", w);

		vertex_t *from = g->get(x);
		vertex_t *to = g->get(y);
		from->v.label = ox;
		to->v.label = oy;

		vertex_t *middle = g->get(ind + cnt);

		arc_t *e1 = g->addedge();
		arc_t *e2 = g->addedge();

		g->bindedge(e1, from, middle);
		g->bindedge(e2, to, middle);
		//e2->v.circ.cost = 1;
		middle->v.circ.bias = -w;
		to->v.circ.bias += w;

		ind++;
	}

	for (uint32_t i = 0; i < dg.nodecnt(); i++) {
		dvertex_t *u = dg.get(i);
		for (uint32_t j = 1; j < u->v.tcnt; j++) {
			uint32_t x = u->v.tstart + j - 1;
			uint32_t y = u->v.tstart + j;

			vertex_t *from = g->get(x);
			vertex_t *to = g->get(y);


			arc_t *e = g->addedge();

			if (r2[i] > r1[i])
				g->bindedge(e, from, to);
			if (r2[i] <= r1[i])
				g->bindedge(e, to, from);
		}
	}





	vertex_t *src = g->get(3*cnt + ecnt);
	for (uint32_t i = 0; i < cnt; i++) {
		vertex_t *v = g->get(i);
        arc_t *e1 = g->addedge();
		arc_t *e2 = g->addedge();

		g->bindedge(e1, src, v);
		g->bindedge(e2, v, src);

		e2->v.circ.cost = 1;
	}

	for (uint32_t i = 0; i < cnt; i++) {
		vertex_t *v = g->get(i);
        arc_t *e1 = g->addedge();
		arc_t *e2 = g->addedge();

		vertex_t *middle = g->get(ind + cnt);

		g->bindedge(e1, v, middle);
		g->bindedge(e2, src, middle);

		ind++;
	}

	for (uint32_t i = 0; i < cnt; i++) {
		vertex_t *v = g->get(i);
        arc_t *e1 = g->addedge();
		arc_t *e2 = g->addedge();

		vertex_t *middle = g->get(ind + cnt);

		g->bindedge(e1, v, middle);
		g->bindedge(e2, src, middle);
		e1->v.circ.cost = 1;

		ind++;
	}

	for (uint32_t i = 0; i < dg.edgecnt(); i++) {
		darc_t *e = dg.get_edge(i);
		uint32_t ox = e->from->id;
		uint32_t oy = e->to->id;
		double p00 = std::max(0.0, rmin[ox] - rmin[oy] + 1);
		double p01 = std::max(0.0, rmin[ox] - rmax[oy] + 1);
		double p11 = std::max(0.0, rmax[ox] - rmax[oy] + 1);
		double w1 = e->v.weight*(p11 - p01);
		double w2 = e->v.weight*(p00 - p01);

		uint32_t x = e->v.from_tindex + e->from->v.tstart;
		uint32_t y = e->v.to_tindex + e->to->v.tstart;

		vertex_t *middle = g->get(ecnt + cnt + x);
		middle->v.circ.bias -= w1;
		src->v.circ.bias += w1;

		middle = g->get(ecnt + 2*cnt + y);
		vertex_t *to = g->get(y);
		middle->v.circ.bias -= w2;
		to->v.circ.bias += w2;

		//printf("%f %f\n", w1, w2);
	}


	return g;
}



double
compute_score(network & g, uint32_t ncnt, uint32_t ecnt)
{
	double agony = 0;
	for (uint32_t i = 0; i < ncnt + ecnt; i++) {
		vertex_t *v = g.get(i);
		agony += v->v.circ.dual*v->v.circ.bias;
	}
	double shift = 0;
	for (uint32_t i = 0; i < ncnt; i++) {
		vertex_t *v = g.get(i);
		shift += v->v.circ.bias;
	}
	return shift - agony;
}

weight_t
compute_flux(weightvector & r1, weightvector & r2)
{
	weight_t r = 0;
	for (uint32_t i = 0; i < r1.size(); i++) {
		r += std::abs(double(r2[i] - r1[i]));
	}
	return r;
}

uint32_t
compute_changepoints(weightvector & r1, weightvector & r2)
{
	uint32_t r = 0;
	for (uint32_t i = 0; i < r1.size(); i++) {
		if (r1[i] != r2[i]) r++;
	}
	return r;
}

void
extract_ranks(network & g, uint32_t ncnt, weightvector & r1, weightvector & r2)
{
	for (uint32_t i = 0; i < ncnt; i++) {
		vertex_t *v = g.get(i);
		r1[i] = v->v.circ.dual;
		//printf("%d\n", r1[i]);
	}
	for (uint32_t i = 0; i < ncnt; i++) {
		vertex_t *v = g.get(i + ncnt);
		r2[i] = v->v.circ.dual;
		//printf("%d\n", r2[i]);
	}
}

void
extract_change(dynamic_network & dg, network & g, doublevector & change)
{
	uintvector change_index(dg.nodecnt());

	for (uint32_t i = 0; i < dg.nodecnt(); i++) {
		dvertex_t *u = dg.get(i);
		change_index[i] = u->v.tcnt / 2;

		for (uint32_t j = 1; j < u->v.tcnt; j++) {
			uint32_t x = u->v.tstart + j;
			uint32_t y = u->v.tstart + j - 1;
			vertex_t *v = g.get(x);
			vertex_t *w = g.get(y);
			//printf("%d\n", v->v.circ.dual);
			if (v->v.circ.dual != w->v.circ.dual)
				change_index[i] = j;
		}
	}

	for (uint32_t i = 0; i < dg.edgecnt(); i++) {
		darc_t *e = dg.get_edge(i);
		if (e->v.from_tindex == change_index[e->from->id])
			change[e->from->id] = e->v.time;
		if (e->v.to_tindex == change_index[e->to->id])
			change[e->to->id] = e->v.time;
	}
}

void
output_ranks(FILE *f, dynamic_network & dg, weightvector & r1, weightvector & r2, doublevector & change)
{
	for (uint32_t i = 0; i < dg.nodecnt(); i++) {
		dvertex_t *u = dg.get(i);
		double t;
		if (std::modf(change[i], &t) == 0.0)
			fprintf(f, "%d %d %d %.0f\n", u->v.label, r1[i], r2[i], change[i]);
		else
			fprintf(f, "%d %d %d %f\n", u->v.label, r1[i], r2[i], change[i]);
	}
}
