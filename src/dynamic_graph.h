#ifndef DYNAMIC_GRAPH
#define DYNAMIC_GRAPH

#include "graph.h"
#include "circulation.h"

struct dnvalue_t;
struct devalue_t;

typedef node_t<dnvalue_t, devalue_t> dvertex_t;
typedef edge_t<dnvalue_t, devalue_t> darc_t;

//typedef node_t<dvertex_t *, darc_t *> dvertexp_t;
//typedef edge_t<dvertex_t *, darc_t *> darcp_t;
//TAILQ_HEAD(darchead, darc_t);



struct
dnvalue_t
{
	dnvalue_t() : tcnt(0), tstart(0), last_time(-std::numeric_limits<double>::infinity()) {}

	uint32_t label;

	uint32_t tcnt;
	uint32_t tstart;
	double last_time;
};

struct
devalue_t
{
	double weight;
	double time;

	uint32_t from_tindex;
	uint32_t to_tindex;

	uint32_t from_rank;
	uint32_t to_rank;
};

typedef graph<dnvalue_t, devalue_t> dynamic_network;

dynamic_network * read(FILE *f, bool weighted, bool self, uint32_t & cnt, uint32_t & ecnt);
network * dynamic_to_linf(dynamic_network  & dg, double lambda, uint32_t limit);
network * dynamic_to_smooth(dynamic_network  & dg, double lambda, uint32_t limit);
network * dynamic_to_smooth(dynamic_network  & dg, doublevector & lambda, uint32_t limit);

network * dynamic_to_split(dynamic_network  & dg, doublevector & change, uint32_t limit);
network * dynamic_to_split(dynamic_network  & dg, weightvector & r1, weightvector & r2);
void init_change(dynamic_network  & dg, doublevector & change);
void extract_ranks(network & g, uint32_t ncnt, weightvector & r1, weightvector & r2);
void extract_change(dynamic_network & dg, network & g, doublevector & change);
void output_ranks(FILE *f, dynamic_network & dg, weightvector & r1, weightvector & r2, doublevector & change);


double compute_score(dynamic_network  & dg, network & g);
double compute_score(network & g, uint32_t ncnt, uint32_t ecnt);
weight_t compute_flux(dynamic_network  & dg, network & g, weightvector & flux);
weight_t compute_flux(weightvector & r1, weightvector & r2);
weight_t compute_maxvar(dynamic_network  & dg, network & g);
uint32_t compute_changepoints(dynamic_network  & dg, network & g);
uint32_t compute_changepoints(weightvector & r1, weightvector & r2);


void output_ranks(FILE *f, dynamic_network & dg, network & g);



#endif

