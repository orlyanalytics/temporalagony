#include "circulation.h"
#include "dynamic_graph.h"
#include <stdio.h>
#include <cmath>

network *
dynamic_to_smooth(dynamic_network  & dg, double lambda, uint32_t limit)
{
	doublevector l(dg.nodecnt(), lambda);
	return dynamic_to_smooth(dg, l, limit);
}

network *
dynamic_to_smooth(dynamic_network  & dg, doublevector & lambda, uint32_t limit)
{
	dvertex_t *last = dg.get(dg.nodecnt() - 1);
	uint32_t cnt = last->v.tstart + last->v.tcnt;
	uint32_t ecnt = dg.edgecnt();
	uint32_t lecnt = cnt - dg.nodecnt();


	// 2* is needed for possible contractions
	network *g = new network(2*(cnt + lecnt + ecnt + 2), 2*(ecnt + lecnt) + 2*cnt + lecnt + 1);

	for (uint32_t i = 0; i < cnt + lecnt + ecnt + 2; i++) {
		vertex_t *v = g->addnode();
		v->v.init(v);
	}

	uint32_t ind = 0;
	for (uint32_t i = 0; i < dg.edgecnt(); i++) {
		darc_t *e = dg.get_edge(i);

		uint32_t x = e->v.from_tindex + e->from->v.tstart;
		uint32_t y = e->v.to_tindex + e->to->v.tstart;
		
		vertex_t *from = g->get(x);
		vertex_t *to = g->get(y);
		from->v.label = e->from->id;
		to->v.label = e->to->id;

		vertex_t *middle = g->get(ind + cnt);

		arc_t *e1 = g->addedge();
		arc_t *e2 = g->addedge();

		g->bindedge(e1, from, middle);
		g->bindedge(e2, to, middle);
		e2->v.circ.cost = 1;
		middle->v.circ.bias = -e->v.weight;
		to->v.circ.bias += e->v.weight;
		//printf("%f\n", e->v.weight);

		ind++;
	}
	//printf("IND: %d\n", ind);

	// lambda edges
	for (uint32_t i = 0; i < dg.nodecnt(); i++) {
		dvertex_t *u = dg.get(i);
		for (uint32_t j = 1; j < u->v.tcnt; j++) {
			uint32_t x = u->v.tstart + j - 1;
			uint32_t y = u->v.tstart + j;
		
			vertex_t *from = g->get(x);
			vertex_t *to = g->get(y);

			vertex_t *middle = g->get(ind + cnt);

			arc_t *e1 = g->addedge();
			arc_t *e2 = g->addedge();

			g->bindedge(e1, from, middle);
			g->bindedge(e2, to, middle);
			middle->v.circ.bias = -2*lambda[i];
			from->v.circ.bias += lambda[i];
			to->v.circ.bias += lambda[i];

			ind++;
		}
	}



	vertex_t *src = g->get(cnt + lecnt + ecnt);
	vertex_t *sink = g->get(cnt + lecnt + ecnt + 1);
	for (uint32_t i = 0; i < cnt; i++) {
        arc_t *e1 = g->addedge();
		arc_t *e2 = g->addedge();

		vertex_t *v = g->get(i);

		g->bindedge(e1, src, v);
		g->bindedge(e2, v, sink);
	}

	arc_t *loop = g->addedge();
	g->bindedge(loop, sink, src);
	if (limit == 0)
		loop->v.circ.cost = cnt - 1;
	else
		loop->v.circ.cost = limit - 1;

	return g;
}

network *
dynamic_to_linf(dynamic_network  & dg, double lambda, uint32_t limit)
{
	dvertex_t *last = dg.get(dg.nodecnt() - 1);
	uint32_t cnt = last->v.tstart + last->v.tcnt;
	uint32_t ecnt = dg.edgecnt();


	// 2* is needed for possible contractions
	network *g = new network(2*(cnt + cnt + ecnt + 2), 2*ecnt + 2*cnt + 2*cnt + 1);

	for (uint32_t i = 0; i < cnt + cnt + ecnt + 2; i++) {
		vertex_t *v = g->addnode();
		v->v.init(v);
	}

	uint32_t ind = 0;
	for (uint32_t i = 0; i < dg.edgecnt(); i++) {
		darc_t *e = dg.get_edge(i);

		uint32_t x = e->v.from_tindex + e->from->v.tstart;
		uint32_t y = e->v.to_tindex + e->to->v.tstart;
		
		vertex_t *from = g->get(x);
		vertex_t *to = g->get(y);
		from->v.label = e->from->id;
		to->v.label = e->to->id;

		vertex_t *middle = g->get(ind + 2*cnt);

		arc_t *e1 = g->addedge();
		arc_t *e2 = g->addedge();

		g->bindedge(e1, from, middle);
		g->bindedge(e2, to, middle);
		e2->v.circ.cost = 1;
		middle->v.circ.bias = -e->v.weight;
		to->v.circ.bias += e->v.weight;
		//printf("%f\n", e->v.weight);

		ind++;
	}

	// constraint edges
	for (uint32_t i = 0; i < dg.nodecnt(); i++) {
		dvertex_t *u = dg.get(i);
		vertex_t *anchor = g->get(cnt + i);
		for (uint32_t j = 0; j < u->v.tcnt; j++) {
			uint32_t x = u->v.tstart + j;
			vertex_t *v = g->get(x);

			arc_t *e1 = g->addedge();
			arc_t *e2 = g->addedge();

			g->bindedge(e1, anchor, v);
			g->bindedge(e2, v, anchor);
			e2->v.circ.cost = lambda;
		}
	}



	vertex_t *src = g->get(cnt + cnt + ecnt);
	vertex_t *sink = g->get(cnt + cnt + ecnt + 1);
	for (uint32_t i = 0; i < cnt; i++) {
        arc_t *e1 = g->addedge();
		arc_t *e2 = g->addedge();

		vertex_t *v = g->get(i);

		g->bindedge(e1, src, v);
		g->bindedge(e2, v, sink);
	}

	arc_t *loop = g->addedge();
	g->bindedge(loop, sink, src);
	if (limit == 0)
		loop->v.circ.cost = cnt - 1;
	else
		loop->v.circ.cost = limit - 1;

	return g;
}

double
compute_score(dynamic_network  & dg, network & g)
{
	double agony = 0;
	for (uint32_t i = 0; i < dg.edgecnt(); i++) {
		darc_t *e = dg.get_edge(i);

		uint32_t x = e->v.from_tindex + e->from->v.tstart;
		uint32_t y = e->v.to_tindex + e->to->v.tstart;
		
		vertex_t *from = g.get(x);
		vertex_t *to = g.get(y);
		agony += e->v.weight * std::max(0, 1 + from->v.circ.dual - to->v.circ.dual);
	}
	return agony;
}



weight_t
compute_flux(dynamic_network  & dg, network & g, weightvector & flux)
{
	weight_t tf = 0;
	for (uint32_t i = 0; i < dg.nodecnt(); i++) {
		dvertex_t *w = dg.get(i);
		flux[i] = 0;
		for (uint32_t j = 1; j < w->v.tcnt; j++) {
			uint32_t x = w->v.tstart + j - 1;
			uint32_t y = w->v.tstart + j;
		
			vertex_t *u = g.get(x);
			vertex_t *v = g.get(y);

			flux[i] += abs(u->v.circ.dual - v->v.circ.dual);
		}
		tf += flux[i];
	}

	return tf;
}

weight_t
compute_maxvar(dynamic_network  & dg, network & g)
{
	weight_t tf = 0;
	weight_t vmin, vmax;

	for (uint32_t i = 0; i < dg.nodecnt(); i++) {
		dvertex_t *u = dg.get(i);

		vmax = vmin = g.get(u->v.tstart)->v.circ.dual;

		for (uint32_t j = 1; j < u->v.tcnt; j++) {
			uint32_t y = u->v.tstart + j;
		
			vertex_t *v = g.get(y);

			vmin = std::min(vmin, v->v.circ.dual);
			vmax = std::max(vmax, v->v.circ.dual);
		}
		tf += vmax - vmin;
	}

	return tf;
}

uint32_t
compute_changepoints(dynamic_network  & dg, network & g)
{
	uint32_t tf = 0;

	for (uint32_t i = 0; i < dg.nodecnt(); i++) {
		dvertex_t *w = dg.get(i);
		for (uint32_t j = 1; j < w->v.tcnt; j++) {
			uint32_t x = w->v.tstart + j - 1;
			uint32_t y = w->v.tstart + j;
		
			vertex_t *u = g.get(x);
			vertex_t *v = g.get(y);

			if (u->v.circ.dual != v->v.circ.dual)
				tf++;
		}
	}

	return tf;
}




void
output_ranks(FILE *f, dynamic_network & dg, network & g)
{
	double t;
	for (uint32_t i = 0; i < dg.nodecnt(); i++) 
		dg.get(i)->v.last_time = -std::numeric_limits<double>::infinity();

	for (uint32_t i = 0; i < dg.edgecnt(); i++) {
		darc_t *e = dg.get_edge(i);

		if (e->from->v.last_time != e->v.time) {
			e->from->v.last_time = e->v.time;
			uint32_t x = e->v.from_tindex + e->from->v.tstart;
			if (std::modf(e->v.time, &t) == 0.0)
				fprintf(f, "%d %.0f %d\n", e->from->v.label, e->v.time, g.get(x)->v.circ.dual);
			else
				fprintf(f, "%d %f %d\n", e->from->v.label, e->v.time, g.get(x)->v.circ.dual);
		}
		if (e->to->v.last_time != e->v.time) {
			e->to->v.last_time = e->v.time;
			uint32_t x = e->v.to_tindex + e->to->v.tstart;
			if (std::modf(e->v.time, &t) == 0.0)
				fprintf(f, "%d %.0f %d\n", e->to->v.label, e->v.time, g.get(x)->v.circ.dual);
			else
				fprintf(f, "%d %f %d\n", e->to->v.label, e->v.time, g.get(x)->v.circ.dual);
		}
		
	}
}
