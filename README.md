# README #

### What is this repository for? ###

Source code for computing agony in temporal networks.

### How do I get set up? ###

* cd src
* make
* ./agony -h
* ./agony -i ../datasets/nhl/nhl_2015_regular_season_bymonth.txt -m c -k 3 -o nhl.out -w
